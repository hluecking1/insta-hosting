
# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %% [markdown]
# ### Hosting instagram pictures

# %%
import os
import sys
currdir = os.getcwd()
parentdir = os.path.abspath(os.path.join(currdir, os.pardir))
sys.path.append(parentdir)
from insta import create_link, create_csv, copy_file
from insta import HostInsta
import creds

# %% [markdown]
# Mare sure you are on remote server: iuser@insta.soz.uni-bielefeld.de

# %%
main_dir = "/var/www/html"
link_folder = os.path.expanduser('~') + "/host_images"


uplink = HostInsta(creds.PGDATABASE, creds.PGUSER, creds.PGHOST, creds.PGPORT, creds.PGPASSWORD, "images")
# Mount fs-big into home-directory! (~/fs-big). Doesnt work for some reason, mount manually
# mount("//fs-big.uni-bielefeld.de/usoz02/kroh_projekt_twitter", "{}/fs-big".format(os.path.expanduser('~')), "hluecking1")
# Create simlink
create_link(main_dir, link_folder)

fileslist = uplink.query_insta("SELECT * from images where image_id > 0 and image_id < 100")

for file_path in fileslist["filename"]:
    file_path = os.path.expanduser('~') + "/fs-big/Instagram" + file_path.split("/insta_stereo/fs-big")[1]  
    copy_file(file_path, link_folder + "/html")

create_csv(fileslist["filename"], "../mturk.csv")


