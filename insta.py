import psycopg2
import atexit
import ctypes
import ctypes.util
import os
import pandas as pd
from psycopg2 import sql
import sys
currdir = os.getcwd()
parentdir = os.path.abspath(os.path.join(currdir, os.pardir))
sys.path.append(parentdir)
import shutil
import creds
import subprocess
import os.path
import sys
import csv

def create_folder(folder):

    proc = subprocess.Popen(['mkdir', folder],
                                    stdin = subprocess.PIPE,
                                    stdout = subprocess.PIPE,
                                    stderr = subprocess.PIPE)

    proc.wait()


def create_link(source, desti):
    
    if not os.path.exists(source): 
        sys.exit('The folder {} does not exist'. format(source))
    if not os.path.exists(desti):
        create_folder(desti)

    ### Will overwrite existing symlink
    proc = subprocess.Popen(['ln', "-sf", source, desti],
                                stdin = subprocess.PIPE,
                                stdout = subprocess.PIPE,
                                stderr = subprocess.PIPE
                            )

    proc.wait()
    

def mount(url, mount_point, username):

    if not os.path.exists(mount_point):
        create_folder(mount_point)

    proc = subprocess.Popen(['sudo', "mount", "-t", "cifs", url, mount_point, "-o", "vers=2.0,username={},domain=ad,dir_mode=0777, file_mode=0777".format(username)],
                                stdin = subprocess.PIPE,
                                stdout = subprocess.PIPE,
                                stderr = subprocess.PIPE
                            )

    proc.wait()
  #  print(out)

def copy_file(source_file, destination_folder):

    proc = subprocess.Popen(['sudo', 'cp', source_file, destination_folder],
                                stdin = subprocess.PIPE,
                                stdout = subprocess.PIPE,
                                stderr = subprocess.PIPE
                            )
    proc.wait()

def create_csv(fileslist, out_file):

    if os.path.exists(out_file):
        proc = subprocess.Popen(['rm', '-f', out_file],
                                stdin = subprocess.PIPE,
                                stdout = subprocess.PIPE,
                                stderr = subprocess.PIPE
                            )
        proc.wait()
    
    with open(out_file, 'w', newline='') as csvfile:
        mturk_csv = csv.writer(csvfile, delimiter=' ')
        mturk_csv.writerow(["image_url"])

        for image in fileslist:
            current = image.split("/")[-1]
            mturk_csv.writerow(["https://insta.soz.uni-bielefeld.de/{}".format(current)])

class HostInsta:

    def __init__(self, database, user, host, port, pwd, table):
        self.table = table
        self.database = database
        try:
            self.conn = psycopg2.connect(host=host, database=self.database, user=user, password=pwd, port=port)
            atexit.register(self.exit_handler)
            self.curs = self.conn.cursor()
            self.curs.execute("Select * FROM {} LIMIT 0".format(self.table))
            self.columns = [desc[0] for desc in self.curs.description]

        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
            print("Make sure your login credentials are correct")        

    def query_insta(self, query):
        self.curs.execute(query)
        rows = self.curs.fetchall()
        results = pd.DataFrame(rows, columns = self.columns)

        return results


    def exit_handler(self):
        print("My application is ending Closing cursors and connections!")
        self.curs.close()
        self.conn.close()


    
